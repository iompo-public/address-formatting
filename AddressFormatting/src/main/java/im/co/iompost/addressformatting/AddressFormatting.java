package im.co.iompost.addressformatting;

import java.util.List;

/**
 * Welcome to the Address Formatting technical question for the Isle of Man Post Office.
 * The purpose of this exercise is to obtain a basic understanding on your abilities with
 * Java & how you may approach some Postal related problems. 
 * 
 * Please note that any failed unit tests or inability to complete any tasks will not have an 
 * effect on the success of your application.
 * 
 * The goal of this task is to store the unformattedAddressList object as formatted Address
 * objects which can be used for the Unit Tests contained within this project. You may create
 * the Address object with any fields or methods you wish. The Unit Tests will call the methods
 * supplied by the AddressOutput interface, so please return the correct responses via those methods.
 * 
 * Thank you in advance for your time & good luck!
 * 
 * @author Isle of Man Post Office
 */
public class AddressFormatting {

    /**
     * String Array of unformatted Addresses to be used as the source data.
     */
    private static final String[] unformattedAddressList = new String[]{
        "28 HOPE STREET,DOUGLAS,ISLE OF MAN,IM1 1AP",
        "10 Peel Road,Douglas,Isle Of Man,IM1 4LR",
        "villa ramos,clay head road,baldrine,isle of man,im4 6dn",
        "21 allan Street,Douglas,Isle of man,IM1 3DJ",
        "Sunnycroft,Ramsey road,Laxey ,Isle of Man,IM4 7PD",
        "Skyr Wyllin,Main Road,Union Mills,Isle Of Man,IM4 4AD",
        "17 hope street,castletown,isle of man, IM9 1as"
    };

    /**
     * List of Address objects which will be used for unit tests.
     */
    private static List<Address> formattedAddesses;

    public static void main(String[] args) {
        System.out.println("Hello World!");
        formatAddresses();
    }

    /**
     * Add any logic into this method to initialise the formattedAddresses List.
     */
    private static void formatAddresses() {
        throw new UnsupportedOperationException("formatAddresses() not supported yet.");
    }

    public static String[] getUnformattedAddressList() {
        return unformattedAddressList;
    }

    public static List<Address> getFormattedAddesses() {
        return formattedAddesses;
    }

}
