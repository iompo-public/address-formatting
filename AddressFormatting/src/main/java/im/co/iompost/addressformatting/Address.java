package im.co.iompost.addressformatting;

/**
 *
 * @author Isle of Man Post Office
 */
public class Address implements AddressOutput {

    /**
     * Return the Postcode of the Address.
     *
     * @return
     */
    @Override
    public String getPostcode() {
        throw new UnsupportedOperationException("getPostcode() not supported yet.");
    }

    /**
     * Return the Town of the Address.
     *
     * @return
     */
    @Override
    public String getTown() {
        throw new UnsupportedOperationException("getTown() not supported yet.");
    }

    /**
     * Return the Country of the Address.
     *
     * @return
     */
    @Override
    public String getCountry() {
        throw new UnsupportedOperationException("getCountry() not supported yet.");
    }

    /**
     * Return a formatted value of all fields of the Address.
     *
     * @return
     */
    @Override
    public String getFormattedAddress() {
        throw new UnsupportedOperationException("getFormattedAddress() not supported yet.");
    }

    /**
     * Return true if the property has a house number.
     *
     * @return
     */
    @Override
    public boolean hasHouseNumber() {
        throw new UnsupportedOperationException("hasHouseNumber() not supported yet.");
    }

    /**
     * Return true if the property is in a City. For the purposes of this
     * exercise, we will assume Douglas & Peel are the only cities.
     *
     * @return
     */
    @Override
    public boolean situatedInACity() {
        throw new UnsupportedOperationException("situatedInACity() not supported yet.");
    }

}
