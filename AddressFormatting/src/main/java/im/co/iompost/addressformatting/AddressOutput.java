package im.co.iompost.addressformatting;

/**
 *
 * @author Isle of Man Post Office
 */
public interface AddressOutput {

    public String getPostcode();

    public String getTown();

    public String getCountry();

    public String getFormattedAddress();

    public boolean hasHouseNumber();
    
    public boolean situatedInACity();

}
