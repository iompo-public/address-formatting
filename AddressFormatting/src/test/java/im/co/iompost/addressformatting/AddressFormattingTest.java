package im.co.iompost.addressformatting;

import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author trowbmat
 */
public class AddressFormattingTest {

    private static String[] unformattedAddresses;
    private static List<Address> formattedAddresses;

    public AddressFormattingTest() {
        String[] args = null;

        try {
            AddressFormatting.main(args);
            unformattedAddresses = AddressFormatting.getUnformattedAddressList();
            formattedAddresses = AddressFormatting.getFormattedAddesses();
        } catch (Exception e) {
            System.out.println("Exception: ".concat(e.getMessage()));
            e.printStackTrace();
        }
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testUnformattedAddressList() {
        assertNotNull(unformattedAddresses);

        assertAll("UnformattedAddresses",
                () -> assertEquals(unformattedAddresses[0].toUpperCase(), "28 HOPE STREET,DOUGLAS,ISLE OF MAN,IM1 1AP"),
                () -> assertEquals(unformattedAddresses[1].toUpperCase(), "10 PEEL ROAD,DOUGLAS,ISLE OF MAN,IM1 4LR"),
                () -> assertEquals(unformattedAddresses[2].toUpperCase(), "VILLA RAMOS,CLAY HEAD ROAD,BALDRINE,ISLE OF MAN,IM4 6DN"),
                () -> assertEquals(unformattedAddresses[3].toUpperCase(), "21 ALLAN STREET,DOUGLAS,ISLE OF MAN,IM1 3DJ"),
                () -> assertEquals(unformattedAddresses[4].toUpperCase(), "SUNNYCROFT,RAMSEY ROAD,LAXEY ,ISLE OF MAN,IM4 7PD"),
                () -> assertEquals(unformattedAddresses[5].toUpperCase(), "SKYR WYLLIN,MAIN ROAD,UNION MILLS,ISLE OF MAN,IM4 4AD"),
                () -> assertEquals(unformattedAddresses[6].toUpperCase(), "17 HOPE STREET,CASTLETOWN,ISLE OF MAN, IM9 1AS")
        );
    }

    @Test
    public void testFormattedAddressList() {
        assertNotNull(formattedAddresses);
        assertEquals(7, formattedAddresses.size());
    }

    @Test
    public void testCountry() {
        assertTrue(formattedAddresses
                .stream()
                .allMatch(address -> address.getCountry().toUpperCase().equals("ISLE OF MAN")));
    }

    @Test
    public void testPostcode() {
        formattedAddresses
                .stream()
                .forEach(address -> assertTrue(address.getPostcode().toUpperCase().matches("^IM\\d{1,2}\\s[0-9][A-Z]{2}$")));
    }

    @Test
    public void testFormattedAddressOutput() {
        formattedAddresses
                .stream()
                .forEach(address -> {
                    assertNotNull(address.getFormattedAddress());
                    System.out.println(address.getFormattedAddress());
                });
    }

    @Test
    public void testHasHouseNumber() {
        long count = formattedAddresses
                .stream()
                .filter(Address::hasHouseNumber)
                .count();

        assertEquals(4L, count);
    }

    @Test
    public void testSituatedInACity() {
        long count = formattedAddresses
                .stream()
                .filter(Address::situatedInACity)
                .count();

        assertEquals(3L, count);
    }

}
