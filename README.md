# Address Formatting

## Description
 Welcome to the Address Formatting technical question for the Isle of Man Post Office. The purpose of this exercise is to obtain a basic understanding on your abilities with Java & how you may approach some Postal related problems. 
 
 Please note that any failed unit tests or inability to complete any tasks will not have an effect on the success of your application.

## Task
 The goal of this task is to store the unformattedAddressList object as formatted Address objects which can be used for the Unit Tests contained within this project. You may modify the Address object with any fields or methods you wish. The Unit Tests will however call the methods supplied by the AddressOutput interface, so please return appripriate responses via those methods.

## Installation
This project is setup to work with Java version 17 or above. You may need to [install or upgrade the JDK on your system](https://openjdk.org/install/). You can find official documentation on how to do this via a web search. To download the project clone the repository to your local development machine.

```bash
git clone https://gitlab.com/iompo-public/address-formatting.git
```
We also recommend you use [Apache NetBeans](https://netbeans.apache.org/download/index.html) as your development environment - this is free downloadable software which you will be using if your application is successful.

After cloning the repository, you can simply open the project & begin developing.

### Linux install
If you are using a Debian based Linux distribution you can setup your environment very quickly. Run the following commands with root access.
```bash
apt update
apt install -y snapd openjdk-17-jdk-headless
snap install netbeans --classic
```
Unfortunately WSL2 doesn't support graphical applications yet so Windows users will need to manually install the software.

## Submission
To submit your project, you may wish to zip the finished project directory & email it. Please make sure to run "clean" on the project prior to zipping to ensure the filesize is kept to a minimum.

You may also wish to host your solution on your own git site (GitLab, GitHub, BitBucket) & provide a public URL via email.

## Support
If you have any questions or concerns, please send us an email & we will be happy to assist.

## Copyright
Copyright Isle of Man Post Office 2023.
